﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Timers;
using System.Net.Http;
using System.Threading.Tasks;

using ApplicationTask = Client.Models.ApiModels.Task;
using ThreadTask = System.Threading.Tasks.Task;
using System.Diagnostics;

namespace Client.Services
{
    public class TaskService
    {
        private readonly HttpClient _client;
        private readonly int _userId;
        private readonly Timer _timer;
        public TaskService(IConfigurationRoot configuration, int userId)
        {
            _client = new HttpClientFactory(configuration).GetClient();
            _userId = userId;
            _timer = new Timer();
        }

        public async Task<int> MarkRandomTaskWithDelay(int delay)
        {
            TaskCompletionSource<int> completionSource = new TaskCompletionSource<int>();

            _timer.Elapsed += async (o, e) => {
                var allTasksResponse = await _client.GetAsync($"user/unifinishedtasks/{_userId}");
                var allTasksResponseContent = await allTasksResponse.Content.ReadAsStringAsync();
                var allTasks = JsonConvert.DeserializeObject<ApplicationTask[]>(allTasksResponseContent);

                if (allTasks.Length == 0)
                {
                    Console.WriteLine($"User does not have unfinished tasks.");
                    completionSource.TrySetResult(0);
                    return;
                }

                var randomizer = new Random();
                var randomTask = allTasks[randomizer.Next(allTasks.Length)];

                var finishTaskResponse = await _client.PutAsync("task/" + randomTask.Id.ToString(), null);
                var finishTaskRepsonseContent = await finishTaskResponse.Content.ReadAsStringAsync();

                if (!finishTaskResponse.IsSuccessStatusCode)
                    completionSource.TrySetException(new Exception($"Something went wrong \n {finishTaskResponse.StatusCode} \n {finishTaskRepsonseContent}"));

                Console.WriteLine($"Task with id {randomTask.Id} was successfully finished");

                completionSource.TrySetResult(randomTask.Id);
            };

            _timer.Interval = delay;
            _timer.AutoReset = false;
            _timer.Enabled = true;

            return await completionSource.Task;
        }
    }
}
