﻿using Client.Models.ApiModels;
using Client.Models.ApiModels.CollectionModels;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using ApplicationTask = Client.Models.ApiModels.Task;
using ThreadTask = System.Threading.Tasks.Task;

namespace Client.Services
{
    public class CollectionService
    {
        private readonly IConfigurationRoot _configuration;
        private readonly IConfigurationSection _collectionEndpointsSection;
        private readonly HttpClient _httpClient;
        public CollectionService(IConfigurationRoot configuration)
        {
            _configuration = configuration;

            _httpClient = new HttpClientFactory(configuration).GetClient();
            _httpClient.BaseAddress = new Uri(_httpClient.BaseAddress + _configuration.GetSection("NetSettings:ApiEndpoints:CollectionEndpoint").Value+"/");
            _collectionEndpointsSection = _configuration.GetSection("NetSettings:ApiEndpoints:CollectionEndpoints");
        }

        /// <summary>
        /// This is solution for 1st task
        /// </summary>
        public async Task<Dictionary<int, int>> GetProjectsIdAndTaskCount(int projectOwnerId)
        {
            var response = await _httpClient.GetAsync(_collectionEndpointsSection.GetSection("ProjectIdAndTaskCountEndpoint").Value+"/"+projectOwnerId);
            
            var content = response.Content;

            await CheckResponse(response);

            try
            {
                var collection = JsonConvert.DeserializeObject<IEnumerable<KeyValue>>(await content.ReadAsStringAsync());
                return collection.ToDictionary(res => res.Key, res => res.Value);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong:");
                Console.WriteLine(ex.Message);
            }

            return default;
        }

        /// <summary>
        /// This is solution for 2nd task
        /// </summary>
        public async Task<IEnumerable<ApplicationTask>> GetUsersTask(int userId)
        {
            var response = await _httpClient.GetAsync(_collectionEndpointsSection.GetSection("UsersTaskEndpoint").Value + "/" + userId);

            var content = response.Content;

            await CheckResponse(response);

            try
            {
                var collection = JsonConvert.DeserializeObject<IEnumerable<ApplicationTask>>(await content.ReadAsStringAsync());
                return collection;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong:");
                Console.WriteLine(ex.Message);
            }

            return default;

        }

        /// <summary>
        /// This is solution for 3d task
        /// </summary>
        public async Task<IEnumerable<TaskIdAndName>> GetFinishedUserTasks(int userId)
        {
            var response = await _httpClient.GetAsync(_collectionEndpointsSection.GetSection("FinishedTaskEndpoint").Value + "/" + userId);

            var content = response.Content;

            await CheckResponse(response);

            try
            {
                var collection = JsonConvert.DeserializeObject<IEnumerable<TaskIdAndName>>(await content.ReadAsStringAsync());
                return collection;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong:");
                Console.WriteLine(ex.Message);
            }

            return default;
        }

        /// <summary>
        /// This is solution for 4th task
        /// </summary>
        public async Task<IEnumerable<TeamWithUsers>> GetTeamsWithUsers()
        {
            var response = await _httpClient.GetAsync(_collectionEndpointsSection.GetSection("TeamsWithUsersEndpoint").Value);

            var content = response.Content;

            await CheckResponse(response);

            try
            {
                var collection = JsonConvert.DeserializeObject<IEnumerable<TeamWithUsers>>(await content.ReadAsStringAsync());
                return collection;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong:");
                Console.WriteLine(ex.Message);
            }

            return default;
        }

        /// <summary>
        /// This is solution for 5th task
        /// </summary>
        public async Task<IEnumerable<UserWithTasks>> GetUsersWithTasks()
        {
            var response = await _httpClient.GetAsync(_collectionEndpointsSection.GetSection("UsersWithTasksEndpoint").Value);

            var content = response.Content;

            await CheckResponse(response);

            try
            {
                var collection = JsonConvert.DeserializeObject<IEnumerable<UserWithTasks>>(await content.ReadAsStringAsync());
                return collection;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong:");
                Console.WriteLine(ex.Message);
            }

            return default;
        }

        /// <summary>
        /// This is solution for 6th task
        /// </summary>
        public async Task<LastProjectAndTaskInfo> GetLastUserProject(int userId)
        {
            var response = await _httpClient.GetAsync(_collectionEndpointsSection.GetSection("LastUserProjectEndpoint").Value+"/"+userId);

            var content = response.Content;

            await CheckResponse(response);

            try
            {
                var collection = JsonConvert.DeserializeObject<LastProjectAndTaskInfo>(await content.ReadAsStringAsync());
                return collection;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong:");
                Console.WriteLine(ex.Message);
            }

            return default;
        }

        /// <summary>
        /// This is solution for 7th task
        /// </summary>
        public async Task<IEnumerable<ProjectAndTeamInfo>> GetProjectWithTeam()
        {
            var response = await _httpClient.GetAsync(_collectionEndpointsSection.GetSection("ProjectWithTeamEndpoint").Value);

            var content = response.Content;

            await CheckResponse(response);

            try
            {
                var collection = JsonConvert.DeserializeObject<IEnumerable<ProjectAndTeamInfo>>(await content.ReadAsStringAsync());
                return collection;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong:");
                Console.WriteLine(ex.Message);
            }

            return default;
        }

        private async ThreadTask CheckResponse(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
                return;

            var message = await response.Content.ReadAsStringAsync();

            throw new Exception(response.StatusCode + ":\t" + message);
        }
    }
}
