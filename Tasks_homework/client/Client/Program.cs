﻿using Client.Services;
using Microsoft.Extensions.Configuration;
using System;

namespace Client
{
    class Program
    {
        static IConfigurationRoot configuration;
        static TaskService taskService;
        static void Main()
        {
            configuration = new ConfigurationBuilder()
           .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
           .AddJsonFile("appsettings.json", false)
           .Build();

            taskService = new TaskService(configuration, 3);

            MarkRandomTaskWithDelay_Wrapper();

            var str = Console.ReadLine();
            Console.WriteLine(str);
        }

        static async void MarkRandomTaskWithDelay_Wrapper(int delay = 1000)
        {
            var markedTaskId = await taskService.MarkRandomTaskWithDelay(delay);
        }
    }
}
